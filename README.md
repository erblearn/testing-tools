# Testing Tools

This repository will be a place to handle some useful tools for unit testing.

Currently, the toolset is small, and will be expanded as needed.

## TestHelper::getPropertyValue($class, $property)

Allows you to retrieve private/protected property values from within a class.

* **@var** *Object $class* - Instance of the class you're retrieving the property from
* **@var** *string $property* - Name of the property 
* **@Throws** a \ReflectionException if the property does not exist
* **@Returns** mixed value of the property

## TestHelper::hasProperty($class, $property)

Allows you to determine if a property exists within a class.

* **@var** *Object $class* - Instance of the class you're checking
* **@var** *string $property* - Name of the property 
* **@Returns** boolean - True if the property exists, false otherwise

## TestHelper::hasMethod($class, $method)

Allows you to determine if a method exists within a class.

* **@var** *Object $class* - Instance of the class you're retrieving the property from
* **@var** *string $property* - Name of the method
* **@Returns** boolean - True if the method exists, false otherwise
