<?php
/**
 * Created by PhpStorm.
 * User: msowers
 * Date: 3/31/15
 * Time: 1:10 PM
 */

namespace ERB\Testing\Tools;


/**
 * Class TestHelper
 *
 * General class to handle some unit testing pieces.
 *
 * @package ERB\Testing\Tools
 */
class TestHelper
{
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * Allows us to retrieve the private/protected variables from a given class and return the property value
     * If the property does not exist, a ReflectionException will bubble up from the ReflectionClass::getProperty
     * method.
     *
     * @param Object $class
     * @param string $propName
     * @return mixed
     *
     *
     */
    public function getPropertyValue($class, $propName)
    {
        $reflection = new \ReflectionClass($class);
        $property = $reflection->getProperty($propName);
        $property->setAccessible(true);
        return $property->getValue($class);
    }

    /**
     * Checks to see if a property exists for a class.  Returns true or false.
     * @param Object $class
     * @param $propName
     * @return bool
     */
    public function hasProperty($class, $propName)
    {
        try {
            $reflection = new \ReflectionClass($class);
            $reflection->getProperty($propName);
            return true;
        } catch (\ReflectionException $e) {
            return false;
        }
    }

    /**
     * Checks to see if a method exists for a class.  Returns true or false.
     * @param Object $class
     * @param $methName
     * @return bool
     */
    public function hasMethod($class, $methName)
    {
        try {
            $reflection = new \ReflectionClass($class);
            $reflection->getMethod($methName);
            return true;
        } catch (\ReflectionException $e) {
            return false;
        }

    }


}
